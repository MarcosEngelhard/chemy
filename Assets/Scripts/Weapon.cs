﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    public Text currentbullettext;
    public Text ammo;
    public int maxcontainer1;
     private int currentcontainer1;

     public int maxcontainer2;
     private int currentcontainer2;

     public int maxcontainer3;
    private int currentcontainer3;

    private List<GameObject> jellylist;
    [SerializeField] public int currentbullet;
    [SerializeField] private Transform firepoint;
    
    [SerializeField] private float holddowntime;
    private float startdowntime;
    private bool _canshoot;
    private GameObject needstobedestroyed;
    [SerializeField] private float timetodisappear;
    [System.Serializable]
    public class Bullets
    {
        public GameObject thebulletprefab;
        public GameObject thespecialbulletprefab;
    }
    public Bullets[] bullets;

    // Start is called before the first frame update
    void Start()
    {
        currentcontainer1 = maxcontainer1;

        currentcontainer2 = maxcontainer2;
        currentcontainer3 = maxcontainer3;
        jellylist = new List<GameObject>();
        _canshoot = true;
        startdowntime = holddowntime;
        currentbullet = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (jellylist.Count != 0)
        {
            Debug.Log("Removido!!");
            if (GameObject.FindGameObjectsWithTag("Trampoline").Length > 3)
            {
                needstobedestroyed = jellylist[0];
                jellylist.RemoveAt(0);
                Destroy(needstobedestroyed);
            }
        }
        if (GameObject.FindGameObjectsWithTag("Trampoline").Length >= 4)
        {
            Debug.Log("Removido!!");
            needstobedestroyed = jellylist[0];
            jellylist.RemoveAt(0);
            Destroy(needstobedestroyed);
            Debug.Log(needstobedestroyed + " Destruido!!");
        }
       // Chancebullet();

        if (Input.GetButtonDown("Fire1") && _canshoot)
        {

            Shoot();

        }
          if (Input.GetMouseButtonDown(1) && _canshoot )
        {
               if ((currentbullet == 0 && currentcontainer1 >= 15) || (currentbullet == 1 && currentcontainer2 >= 15) || (currentbullet == 2 && currentcontainer3 >= 15))
            {
                      SpecialShoot();
            }


        }

        if(Input.GetMouseButtonDown(1) && _canshoot  && currentbullet != 0)
        {
            SpecialShoot();
        }
        Chancebullet();



    }
    void Shoot()
    {
        Instantiate(bullets[currentbullet].thebulletprefab, firepoint.position, firepoint.rotation);
        if (currentbullet == 0)
        {
            currentcontainer1--;
        }
        if (currentbullet == 1)
        {
            currentcontainer2--;
        }
        if (currentbullet == 2)
        {
           currentcontainer3--;
        }
        if (currentcontainer2 != 2)
        {
            StartCoroutine("Betweenshoot");
        }


    }
    void SpecialShoot()
    {
        GameObject bullet = (GameObject)Instantiate(bullets[currentbullet].thespecialbulletprefab, firepoint.position, firepoint.rotation);
        if (currentbullet == 0)
        {
            Debug.Log("Adicionado");
            jellylist.Add(bullet);
            currentcontainer1 -= 15;
        }
        else if (currentbullet == 1)
        {
            StartCoroutine(Timertodisappear(bullet));
            currentcontainer2 -= 15;
        }
        else if (currentbullet == 2)
        {
            currentcontainer3 -= 15;
        }
        holddowntime = startdowntime;
        StartCoroutine("Betweenshoot");
    }
    IEnumerator Betweenshoot()
    {
        Debug.Log("Não podes disparar");
        _canshoot = false;
        yield return new WaitForSeconds(0.25f);
        _canshoot = true;
        Debug.Log("Podes disparar");
    }
    IEnumerator Timertodisappear(GameObject bullet)
    {
        Debug.Log("liquid to dissapear!!");
        yield return new WaitForSeconds(timetodisappear);
        Destroy(bullet);
    }
    void Chancebullet()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // pode mudar a bala com o rato
        {
            if (currentbullet >= (bullets.Length - 1)) // se estiver no limite, a arma selecionada irá voltar para a inicial
                currentbullet = 0;

            else
                currentbullet++; // se não, incrementa na variável
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f) // com o scroll do taro para baixo
        {
            if (currentbullet <= 0)
                currentbullet = 2; // ira mudar na ordem decresente
            else
                currentbullet--; // se não, desincremente na variável
        }
        // também poderás trocar as armas nas teclas
        if (Input.GetKeyDown(KeyCode.Alpha1)) // tecla 1 
        {

            currentbullet = 0; // arma primária
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && bullets.Length >= 2) // tecla 2
        {

            currentbullet = 1; // arma secundário
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && bullets.Length >= 3) // tecla 3
        {

            currentbullet = 2; // arma terciária
        }
        if(currentbullet == 0)
        {
            currentbullettext.text = "Jelly";
            ammo.text = currentcontainer1.ToString() + "/" + maxcontainer1.ToString();
        }
        if (currentbullet == 1)
        {
            currentbullettext.text = "Niquidnitrogen";
            ammo.text = currentcontainer2.ToString() + "/" + maxcontainer2.ToString();
        }
        if (currentbullet == 2)
        {
            currentbullettext.text = "Sulfur";
            ammo.text = currentcontainer3.ToString() + "/" + maxcontainer3.ToString();
        }

    }
    public void HowmanyJells()
    {
        if (jellylist.Count != 0)
        {

            if (GameObject.FindGameObjectsWithTag("Trampoline").Length > 3)
            {
                Debug.Log("Removido!!");
                needstobedestroyed = jellylist[0];
                jellylist.RemoveAt(0);
                Destroy(needstobedestroyed);
                Debug.Log(needstobedestroyed + " Destruido!!");
            }
        }
    }
    public void Reload(ref int currentcontainer, ref int maxcontainer, ref int reserveammo)
    {
        if (reserveammo >= maxcontainer)
        {
            reserveammo -= maxcontainer;
            currentcontainer = maxcontainer;
        }
        else if (reserveammo != 0)
        {
            currentcontainer = reserveammo;
            reserveammo -= reserveammo;
        }
        else
        {
            Debug.Log("Sem munição");
        }
        Debug.Log("Currentammo = " + currentcontainer + "armazenamento de reservo = " + reserveammo);

    }
    public void Getammoforfirstbullet()
    {
        currentcontainer1 += 50;
        if (currentcontainer1 > maxcontainer1)
        {
            currentcontainer1 = maxcontainer1;
        }
        Debug.Log("munição adicionado");
    }
    public void Getammoforsecondbullet()
    {

        currentcontainer2 += 50;
         if (currentcontainer2 > maxcontainer2)
        {
            currentcontainer2 = maxcontainer2;
        }
    }
    public void Getammoforthethirdbullet()
    {
        currentcontainer3 += 50;
         if (currentcontainer3 > maxcontainer3)
        {
                 currentcontainer3 = maxcontainer3;
        }
    }
}
