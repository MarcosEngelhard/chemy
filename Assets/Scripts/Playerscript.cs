﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playerscript : MonoBehaviour
{
    //private Animator animator;
    private CharacterController characterController;
    private CapsuleCollider capsule;
    [SerializeField] private float _speed =7f;
    [SerializeField] private float _sprintspeed;
     public float _gravity;
    [SerializeField] private float _jumpheigh;  
    [SerializeField] private Transform hookshoottransform;
    private Vector3 charactervelocitymomentum;
    public Transform groundcheck;
    public float grounddistance;
    public LayerMask groundmask;
    private float firsgravity;
    private Vector3 velocity;
    private bool _isgrounded;
    private Rigidbody rb;
    public Camera playercamera;
    private State state;
    private Vector3 hookshootposition;
    private float hookshootsize;
    [SerializeField] private LayerMask _whatcanhook;
    public float maxhookdistance;
    private Vector3 move;
    private bool isjumping;
    public float fallmultiplier;  
    private bool _iscrouched;
    public Transform Attackpoint;
    public float attackrange;
    public LayerMask enemymask;
    public float attackrate;
    private float nextattacktime;
    private string whathit;
    Enemymove enemy;
    public float pushpowerhook;
    public float pushstrength;
    public Image fullhp;
    public Image midhp;
    public Image lowhp;
    [SerializeField] private float health = 100;
    private enum State
    {
        Normal,
        Hookshootingthrown,
        Hookshootingplayer,
    }

    // Start is called before the first frame update
    void Start()
    {
        //animator = GetComponent<Animator>();
        capsule = GetComponent<CapsuleCollider>();
        characterController = GetComponent<CharacterController>();
        characterController.enabled = false;
        rb = GetComponent<Rigidbody>();
        firsgravity = _gravity;
        hookshoottransform.gameObject.SetActive(false);
        _iscrouched = false;
        isjumping = false;
        
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        
        Rigidbody hitbody = hit.collider.attachedRigidbody;
        if (hitbody == null || hitbody.isKinematic)
        {
            return;
        }
        if(hit.moveDirection.y < -0.1f)
        {
            return;
        }
        if(hit.collider.gameObject.layer == 0 && hit.collider.CompareTag("Trampoline"))
        {
            Vector3 direction = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
            hitbody.velocity = direction * pushstrength;
            Debug.Log(direction);
        }
        
    }
    // Update is called once per frame
    void Update()
    {     
        switch (state)
        {
            default:
            case State.Normal:
                Movecharacter();
                Handlehookstart();
                break;
            case State.Hookshootingthrown:
                Handlehookatthrown();
                Movecharacter();
                break;

            case State.Hookshootingplayer:
                HandleHookShootMovement();
                break;
        }
        //_isgrounded = false;
    }
    private void Movecharacter()
    {
        _isgrounded = Physics.Raycast(transform.position, Vector3.down, capsule.bounds.extents.y+ 0.3f,groundmask);
        if(_isgrounded)
        {
            isjumping = false;
        }
        
        /*if(_isgrounded && velocity.y < 0) // saltar
        {
            
            velocity.y = 0;
        }*/

        float x = Input.GetAxisRaw("Horizontal");
        float z = Input.GetAxisRaw("Vertical");
       
        move = (transform.right * x + transform.forward * z).normalized;
        if(move.x !=0 || move.z != 0)
        {
            StopCoroutine("Stopmoving");
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (_iscrouched)
            {
                
                _iscrouched = false;
                //transform.localScale = new Vector3(transform.localScale.x, normalplayerheight, transform.localScale.z);
            }
            else
            {
                
                _iscrouched = true;
                //transform.localScale = new Vector3(transform.localScale.x, ((float)normalplayerheight/2), transform.localScale.z);
            }
        }
        
        if (Input.GetKey(KeyCode.LeftShift))
        {                                
            if (rb.velocity.x > _sprintspeed || rb.velocity.z > _sprintspeed)
            {
                rb.velocity = Vector3.ClampMagnitude(rb.velocity, _sprintspeed);
            }
            else
            {
                rb.AddForce(move * _sprintspeed, ForceMode.Force);
            }
        }
        else
        {
            if (rb.velocity.x > _speed || rb.velocity.z > _speed)
            {
               
                rb.velocity = Vector3.ClampMagnitude(rb.velocity, _speed);
            }
            else
            {
                rb.AddForce(move * _speed, ForceMode.Force);
            }
        }
        if (Testinputjump())
        {
            if (_isgrounded)
            {
                StopCoroutine("Stopmoving");
                rb.AddForce(Vector3.up * _jumpheigh, ForceMode.Impulse);
                isjumping = true;
            }                       
        }
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (fallmultiplier - 1) * Time.deltaTime;
        }
        velocity.y += _gravity * Time.deltaTime;
        //apply momentum
        velocity += charactervelocitymomentum;              
        //dumpen momentum
        //characterController.Move(move * _speed * Time.deltaTime);
        if (charactervelocitymomentum.magnitude >= 0f)
        {
            float momentumdrag = 3f;
            charactervelocitymomentum -= charactervelocitymomentum * momentumdrag * Time.deltaTime;
            if (charactervelocitymomentum.magnitude <= 0f)
            {
                charactervelocitymomentum = Vector3.zero;
            }
        }
        else if (charactervelocitymomentum.magnitude <= 0f)
        {
            charactervelocitymomentum = Vector3.zero;
        }
        //Debug.Log(rb.velocity);
        if(Time.time >= nextattacktime)
        if (Input.GetKeyDown(KeyCode.F))
        {
            Meeleattack(); // Meele attack;
            nextattacktime = Time.time + 1f / attackrate;
        }
    }
    private void Handlehookstart()
    {
        if (TestinputDownhookshoot())
        {         
            if (Physics.Raycast(playercamera.transform.position, playercamera.transform.forward, out RaycastHit raycasthit ,Mathf.Infinity ,_whatcanhook)) // Hit something;
            {
                //Debug.Log(raycasthit.collider.gameObject.tag);
                enemy = raycasthit.collider.GetComponent<Enemymove>();
                //whathit = raycasthit.collider.gameObject.tag;
                hookshootposition = raycasthit.point;
                Debug.Log(raycasthit.collider.name);
                hookshootsize = 0f; // forºa o tamanho do hook a 0
                hookshoottransform.gameObject.SetActive(true);
                hookshoottransform.localScale = Vector3.zero;
                state = State.Hookshootingthrown;                               
            }            
        }
    }
    private void Handlehookatthrown()
    {
        hookshoottransform.LookAt(hookshootposition);
        float hookshottrowedspeed = 30f;
        hookshootsize += hookshottrowedspeed * Time.deltaTime;
        hookshoottransform.localScale = new Vector3(1, 1, hookshootsize);
        if (hookshootsize > maxhookdistance)/*Se o hook chegar a sua maxdistancia para de ser lançado*/
        {
            Stophookshoot();
        }
        if(hookshootsize >= Vector3.Distance(transform.position, hookshootposition)) // se o hook chegou ao destino
        {
            if(enemy != null) // se hokou contra um inimigo
            {
                //float pushpower = 5f;
                Vector3 pusdir = (  transform.position - enemy.transform.position );
                enemy.Pushedbyhook(pusdir, pushpowerhook);              
                Stophookshoot();
            }
            else
            {
                state = State.Hookshootingplayer;
            }           
        }
    }
    private void Resetgravityeffect()
    {
        velocity.y = 0;
    }
    private void HandleHookShootMovement()
    {
        hookshoottransform.LookAt(hookshootposition);
        float hookshottrowedspeed = 30f;
        hookshootsize -= hookshottrowedspeed * Time.deltaTime;
        hookshoottransform.localScale = new Vector3(1, 1, hookshootsize); // aumenta o tamanho do hook
        Vector3 hookshootdirection = (hookshootposition - transform.position).normalized;
        float hookshotmin = 10f;
        float hookshotmax = 30f;
        float hookshotspeed = Mathf.Clamp( Vector3.Distance(transform.position, hookshootposition), hookshotmin, hookshotmax); // limitar a velocidade
        float hookshoptmultiplier = 2f;
        // Move com o  charracter controller
        characterController.enabled = true;
        characterController.Move(hookshootdirection * hookshotspeed * hookshoptmultiplier* Time.deltaTime);
        rb.AddForce( hookshootdirection * hookshotspeed * hookshoptmultiplier * Time.deltaTime);
        float reachedhookshootdistance = 3f;       
        if((Vector3.Distance(transform.position, hookshootposition) < reachedhookshootdistance)) 
        {
            // Reached hookshock position;
            Stophookshoot();
            characterController.enabled = false;         
        }
        if (TestinputDownhookshoot())
        {
            // Cancel hookshot;
            Stophookshoot();
            characterController.enabled = false;
        }
        if (Testinputjump())
        {
            //Cancel with Jump;
            characterController.enabled = false;
            Stophookshoot();
            velocity.y = Mathf.Sqrt(_jumpheigh*10 * -2f * _gravity);                      
        }       
    }
    private void Stophookshoot() // para o hook
    {
        state = State.Normal;        
        Resetgravityeffect();
        hookshoottransform.gameObject.SetActive(false);
        characterController.enabled = false;
    }
    private bool TestinputDownhookshoot()
    {
        return Input.GetKeyDown(KeyCode.E);
    }
    private bool Testinputjump()
    {        
        return Input.GetButtonDown("Jump");
    }
    private void OnTriggerEnter(Collider other) // apanha a munição para as respetivas armas
    {
        if (other.gameObject.CompareTag("Ammo1"))
        {           
            Weapon weapon =(Weapon) FindObjectOfType(typeof(Weapon));
            Debug.Log(weapon.name);
           weapon.Getammoforfirstbullet();
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Ammo2"))
        {           
            Weapon weapon = (Weapon)FindObjectOfType(typeof(Weapon));
            Debug.Log(weapon.name);
            weapon.Getammoforsecondbullet();
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Ammo3"))
        {          
            Weapon weapon = (Weapon)FindObjectOfType(typeof(Weapon));
            Debug.Log(weapon.name);
           weapon.Getammoforthethirdbullet();
            Destroy(other.gameObject);
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground") && !isjumping && (move.x == 0 && move.z == 0))
        {
            StartCoroutine("Stopmoving"); // Courotine para forçar o player a parar
        }
    }  
    public void Resetgravity()
    {
        _gravity = firsgravity;
    }
    IEnumerator Stopmoving()
    {
        yield return new WaitForSeconds(0.25f);
        rb.velocity = Vector3.zero; // Forçar o player a parar.
    }
    public void Meeleattack() // O Player irá atacar com o melee
    {
        Collider[] hitenemies = Physics.OverlapSphere(Attackpoint.position, attackrange, enemymask); // criar um array em todos os inimigos que tocou
        foreach(Collider enemy in hitenemies)
        {
            Debug.Log("We hit " + enemy.name);
            enemy.GetComponent<Enemymove>().Takedamage(100);
        }
    }
    private void OnDrawGizmosSelected()
    {
        if(Attackpoint == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(Attackpoint.position, attackrange);
    }
    public void Playertakedamage(float amountdamage)
    {
        health -= amountdamage;
        if(health <= 60)
        {
            fullhp.gameObject.SetActive(false);
            midhp.gameObject.SetActive(true);
            lowhp.gameObject.SetActive(false);
        }
        if(health <= 20)
        {
            fullhp.gameObject.SetActive(false);
            midhp.gameObject.SetActive(false);
            lowhp.gameObject.SetActive(true);
        }
    }
}
