﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private Type type;
    public enum Type
    {
        Jelly,
        liquidNitrogen,
        Sulfur,
        Normal
    }
    
    public Rigidbody _rb;
    public float speed;
    private bool tookdamage;
    public float damage;
    
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        tookdamage = false;
    }

    // Update is called once per frame
    void Update()
    {
        _rb.velocity = transform.forward * speed;
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Enemymove enemy = other.GetComponent<Enemymove>();
        if(enemy != null && !tookdamage)
        {
            Debug.Log("Enemy!!");
            if(type == Type.Jelly)
            {
                enemy.Slowed();
            }
            if(type == Type.liquidNitrogen)
            {

                enemy.Freezing();
            }
            
            enemy.Takedamage(damage);
            tookdamage = true;
        }
        Destroy(gameObject);
        Playerscript playerscript = other.GetComponent<Playerscript>();
        if(playerscript != null && type == Type.Normal)
        {
            playerscript.Playertakedamage(damage);
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        
        Destroy(gameObject);
    }
}
