﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    private bool bounce;
    public GameObject player;
    public Playerscript playerscript;
    private bool jumphigher;
    private bool playerontrapoline;
    private bool _isjumping;
    [SerializeField] private float _jumppressremeber; //  dentro disso pode dar um salto maior
    private float _jumppressremebertime =4f;
    [SerializeField] private float maxdistance;
    private bool _isfrozen;
    public float initialtimeunfreeze;
    private float currenttimetounfreeze;
    private string initialtag;
    private Rigidbody rb;
    private float initialyvalue;
    // Start is called before the first frame update
    void Start()
    {
        currenttimetounfreeze = initialtimeunfreeze;
        _isfrozen = false;
        _isjumping = false;
        bounce = false;
        jumphigher = false;
        playerontrapoline = false;
        player = GameObject.FindGameObjectWithTag("Player");
        playerscript = player.GetComponent<Playerscript>();
        initialtag = transform.tag;
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        initialyvalue = transform.position.y;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            
            bounce = true;
            player = collision.gameObject;
            playerontrapoline = true;           
        }
        if (collision.gameObject.CompareTag("LiquidNitrogen"))
        {
            if (!_isfrozen)
            {
                _isfrozen = true;
                transform.tag = "Ground";
                transform.gameObject.layer = 9;
                rb.isKinematic = false;
                Debug.Log("isfrozen");
                
            }           
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {           
            bounce = true;
            player = collision.gameObject;
        }
    }   
    // Update is called once per frame
    void Update()
    {      
        if (Mathf.Abs(player.transform.position.x - transform.position.x) >= maxdistance && playerontrapoline)
        {           
            Debug.Log("Player muito longe");
            playerontrapoline = false;
        }
        if (_isfrozen)
        {
            if(currenttimetounfreeze <= 0)
            {
                _isfrozen = false;
                transform.tag = initialtag;
                rb.isKinematic = true;               
                currenttimetounfreeze = initialtimeunfreeze;
            }
            currenttimetounfreeze -= Time.deltaTime;
        }
        if (!_isfrozen)
        {
            if (jumphigher)
            {
                _jumppressremeber -= Time.deltaTime;
            }
            if (_jumppressremeber < 0)
            {
                _jumppressremeber = 0;
                jumphigher = false;
            }
            Spacein();
            if (bounce && jumphigher && _jumppressremeber > 0)
            {
                _isjumping = true;
                Debug.Log("Aquiiiiiiiiii");

                player.GetComponent<Rigidbody>().velocity = new Vector3(0, 20f, 0);

                StartCoroutine("resetnotjumphigher");
                _jumppressremeber = 0;
                playerontrapoline = false;

            }
            else if (bounce && !jumphigher && !_isjumping)
            {
                Debug.Log("Não!!");
                playerscript.Resetgravity();
                player.GetComponent<Rigidbody>().velocity = new Vector3(0, 10f, 0);
                bounce = false;
            }
        }                
    }
    void Spacein() 
    {
        if (Input.GetKeyDown(KeyCode.Space) && !jumphigher && playerontrapoline)
        {
            Debug.Log("Saltamaisalto");
            jumphigher = true;
            _jumppressremeber = _jumppressremebertime;
            
        }
    }
    IEnumerator resetnotjumphigher()
    {
        yield return new WaitForSeconds(2f);
        jumphigher = false;
        _jumppressremeber = 0;
        
        playerontrapoline = false;
        bounce = false;
        _isjumping = false;
    }
}
