﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemymove : MonoBehaviour
{
    public enum TypeofSlime
    {
        Shooter, Meele
    }
    [SerializeField] private TypeofSlime typeofSlime;
    [SerializeField]
    private Playerscript _player;
    private NavMeshAgent _navmeshagent;
    private float currenthealth;
    public float maxhealth;
    [SerializeField] private float targetrange;
    private bool _isslowed;
    private float normalspeed;
     private float slowtime;
    private bool _isfrozen;
    private float frozentime;
    private Vector3 initialposition;
    public GameObject enemybullet;
    [SerializeField] private float attackrange;
    [SerializeField] private float timebetweenshoots;
    private bool _canshoot;
    [SerializeField] private LayerMask playerlayer;
    private bool _canhit;
    private Rigidbody _rb;
    private bool playerinsight;
    private SphereCollider col;
    private Vector3 direction;
    [SerializeField]private float enemyfieldview;
    public List<GameObject> Patrolpoints; // a lista de todos os nodes de patrulha para visitar
    private int _currentpatrolindex;
    
    
    private bool _patrolwaiting; // ditacts wether (determina)  o agente espera em cada node
    
    [SerializeField] private float _totalwaittimepatrol; // o tempo total em que esperamos
     private float waitpatroltimer;
    [SerializeField] private float _switchprobability; // a probabilidade de mudar direção
    private enum State
    {
        Idle,Patrol, ChaseTarget, BacktoSpawn,Shoot,Meele
    }
    private State state;
    // Start is called before the first frame update
    void Start()
    {
        playerinsight = false;
        initialposition = transform.position;
        state = State.Idle;
        currenthealth = maxhealth;
        col = GetComponent<SphereCollider>();
        _navmeshagent = this.GetComponent<NavMeshAgent>();
        _rb = GetComponent<Rigidbody>();
        normalspeed = _navmeshagent.speed;
        _isslowed = false;
        _isfrozen = false;
        _canshoot = true;
        _canhit = true;
        if(_navmeshagent == null)
        {
            Debug.Log("No navmeshagent attached to " + gameObject.name);
        }
        _player = FindObjectOfType<Playerscript>();
        waitpatroltimer = _totalwaittimepatrol;
        _currentpatrolindex = Random.Range(0, Patrolpoints.Count);
    }

    // Update is called once per frame
    void Update()
    {
        if (_isslowed)
        {
            slowtime -= Time.deltaTime;
            if (slowtime <= 0)
            {
                Debug.Log("Já não está mais slow");
                _navmeshagent.speed = normalspeed;
                _isslowed = false;
            }
            
        }
        if (_isfrozen)
        {
            frozentime -= Time.deltaTime;
            if(frozentime <= 0)
            {
                Debug.Log("Inimigo já se descongelou...");
                _navmeshagent.speed = normalspeed;
                _isfrozen = false;
                
            }
            return;
        }
        if (!_isfrozen)
        {
            switch (state)
            {
                default:
                case State.Idle:
                    Checktoplayerdistance();
                    break;
                case State.Patrol:
                    
                    break;
                case State.ChaseTarget:
                    Chaseplayer();
                    break;
                case State.BacktoSpawn:
                    Totheoriginalplace();
                    break;
                case State.Shoot:
                    Shootingtotheplayer();
                    break;
                case State.Meele:
                    Meelingtheplayer();
                    break;
            }
        }
        
        
    }
    private void Checktoplayerdistance()
    {
        Debug.Log("A patrulhar... a patrulhar...");
        _navmeshagent.SetDestination(Patrolpoints[_currentpatrolindex].transform.position);
        if(_navmeshagent.remainingDistance <= 0.2f)
        {
            if(waitpatroltimer <= 0)
            {
                _currentpatrolindex = Random.Range(0, Patrolpoints.Count);
                waitpatroltimer = _totalwaittimepatrol;
            }
            else
            {
                waitpatroltimer -= Time.deltaTime;
            }
        }

        IsPlayeronvieldofsight();
              
        if (Vector3.Distance(transform.position, _player.transform.position) < targetrange && playerinsight)
        {            
            if (_player != null)
            {
                waitpatroltimer = _totalwaittimepatrol;
                state = State.ChaseTarget;
                
            }
        }        
    }
    
    
    private void Chaseplayer()
    {
        
        if(Vector3.Distance(transform.position, _player.transform.position) < targetrange) // Irá em direçaõ ao jogador
        {
            if (_player != null)
            {
                StopCoroutine("Idleintostartposition");
                _navmeshagent.isStopped = false;
                Vector3 targetvector = _player.transform.position;
                Vector3 lookvector = _player.transform.position - transform.position;
                Quaternion rot = Quaternion.LookRotation(lookvector);
                transform.rotation = Quaternion.Slerp(transform.rotation, rot, 1);
                _navmeshagent.SetDestination(targetvector); // Está a mover-se em direção ao player
            }
        }
        
        if(Vector3.Distance(transform.position, _player.transform.position) > targetrange) // ao estar longe do player
        {
            Debug.Log("Distância entre o inimigo e o jogador muito longe...");
            _navmeshagent.isStopped = true;
            StartCoroutine("Idleintostartposition"); 
            
        }
        if(Vector3.Distance(transform.position, _player.transform.position) <= attackrange && typeofSlime == TypeofSlime.Shooter) // em posição de ataque.
        {
            state = State.Shoot;
        }
        if (Vector3.Distance(transform.position, _player.transform.position) <= attackrange && typeofSlime == TypeofSlime.Meele) // em posição de ataque.
        {
            state = State.Meele;
        }
    }
    private void Totheoriginalplace()
    {
        if(initialposition != null)
        {
            _navmeshagent.SetDestination(initialposition);
        }
        if(Vector3.Distance(transform.position,initialposition) <= 1)
        {
            state = State.Idle;
        }
        IsPlayeronvieldofsight();
        if (Vector3.Distance(transform.position, _player.transform.position) < targetrange && playerinsight) // encontrar o player dentro da zona
        {
            state = State.ChaseTarget;

        }
    }
    private void Shootingtotheplayer()
    {
        if (Vector3.Distance(transform.position, _player.transform.position) < targetrange)
        {
            _navmeshagent.isStopped = true;
            StopCoroutine("Idleintostartposition");
            Vector3 lookvector = _player.transform.position - transform.position;
            Quaternion rot = Quaternion.LookRotation(lookvector);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, 1); // estar virado de frente para o player;
            if (_canshoot)
            {
                Debug.Log("Tiroooo!!");
                Instantiate(enemybullet, transform.position, transform.rotation);
                StartCoroutine("Nextshoot");
            }
        }      
        if (Vector3.Distance(transform.position, _player.transform.position) > targetrange)
        {
            Debug.Log("Distância entre o inimigo e o jogador muito longe...");
            StartCoroutine("Idleintostartposition");

        }
    }
    private void Meelingtheplayer()
    {
        if (_canhit)
        {
            Collider[] hitplayer = Physics.OverlapSphere(transform.position, attackrange, playerlayer);
            foreach (Collider player in hitplayer)
            {
                Debug.Log("We hit " + player.name);
                StartCoroutine("Nexthit");
            }
        }
        IsPlayeronvieldofsight();
        if (Vector3.Distance(transform.position, _player.transform.position) > attackrange && playerinsight)
        {
            state = State.ChaseTarget;

        }
        

    }
    public void Takedamage(float amountdamage)
    {
        currenthealth -= amountdamage;
        if(currenthealth <= 0)
        {
            Die();
        }
    }
    public void Die()
    {
        Destroy(gameObject);
    }
    public void Slowed()
    {
        if (!_isslowed)
        {
            _isslowed = true;
            _navmeshagent.speed -= 2.5f;// o inimigo fica mais devagar;
            Debug.Log("Inimigo a andar devagar");
            slowtime = 5f;
        }       
    }
    public void Freezing()
    {
        if (!_isfrozen)
        {
            _isfrozen = true;
            _navmeshagent.speed = 0f; // o inimigo fica parado
            Debug.Log("Inimigo parado");
            frozentime = 3f;
        }
    }
    public void Pushedbyhook(Vector3 pushdirection, float pushpower)
    {
        _rb.AddForce(pushdirection * pushpower);
        StartCoroutine("Stoptheforcebythehook");
    }
    public IEnumerator Idleintostartposition()
    {
        state = State.Idle;
        Debug.Log("Fica em idle por algum tempo...");
        
        yield return new WaitForSeconds (6f); // primeiramente fica no sitio e só depois voltar para a sua posição inicial
        Debug.Log("Depois devia voltar para o sitio inicial");
        state = State.BacktoSpawn;
        _navmeshagent.isStopped = false;
    }
    IEnumerator Nextshoot()
    {
        _canshoot = false;
        yield return new WaitForSeconds(timebetweenshoots); // tempo de espera para depois lançar a próxima bala/gosma
        _canshoot = true;
    }
    IEnumerator Nexthit()
    {
        _canhit = false;
        yield return new WaitForSeconds(timebetweenshoots);
        _canhit = true;
    }
    IEnumerator Stoptheforcebythehook()
    {
        yield return new WaitForSeconds(4f);
        _rb.velocity = Vector3.zero;
        
    }
    private void IsPlayeronvieldofsight()
    {
        direction = _player.transform.position - transform.position; // direção do raycast em que aponto o player;
        float angle = Vector3.Angle(direction, transform.forward);
        if (angle < enemyfieldview * 0.5f) // se o player estiver dentro do angulo de visão do inimigo
        {

            RaycastHit hit; // para ver o que deteta
            if (Physics.Raycast(transform.position + transform.up, direction.normalized, out hit, targetrange)) //colide algo 
            {
                Debug.Log("Aquii");
                if (hit.collider.CompareTag("Player")) // se o player estiver na visão do inimigo
                {
                    playerinsight = true;

                }
                else // algo estiver entre o inimigo e o player
                {
                    playerinsight = false;
                }
            }
        }
    }
   
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(transform.position + transform.up, direction.normalized);
        
        
    }

}
