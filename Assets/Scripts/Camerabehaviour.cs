﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camerabehaviour : MonoBehaviour
{
    public float mousesensitivity;
    public Transform playerbody;
    private float _xrotation;
    // Start is called before the first frame update
    void Start()
    {
        _xrotation = 0;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        Looking();
    }
    private void Looking()
    {
        float mouseX = Input.GetAxis("Mouse X") * mousesensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mousesensitivity * Time.deltaTime;
        _xrotation -= mouseY;
        _xrotation = Mathf.Clamp(_xrotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(_xrotation, 0f, 0f);
        playerbody.Rotate(Vector3.up * mouseX);
    }
}
