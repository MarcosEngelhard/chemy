﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jellyspecial : MonoBehaviour
{
    private GameObject weapon;
    private Weapon weaponscript;
    
    public enum SpecialType
    {
        Jelly,
        liquidNitrogen,
        Sulfur
    }
    [SerializeField] private SpecialType specialtype;
    public Rigidbody _rb;
    public float speed;
    Vector3 intotrampoline;
    Vector3 intosliding;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        weapon = GameObject.FindGameObjectWithTag("Weapon");
        weaponscript = weapon.GetComponent<Weapon>();
    }
    // Update is called once per frame
    void Update()
    {
        _rb.velocity = transform.forward * speed;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground") && specialtype == SpecialType.Jelly)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            intotrampoline = transform.localScale;
            intotrampoline.x = 15;
            intotrampoline.y = 2;
            intotrampoline.z = 15;
            transform.localScale = intotrampoline;
            weaponscript.HowmanyJells();
            GetComponent<Trampoline>().enabled = true;
            gameObject.layer = 0;
            //GetComponent<Rigidbody>().isKinematic = true;
            transform.position += new  Vector3(0, (float) (transform.localScale.y/2f) + 0);
            Destroy(GetComponent<Jellyspecial>());
            
        }
        else if (!collision.gameObject.CompareTag("Ground") && specialtype == SpecialType.Jelly)
        {
            Destroy(gameObject);
            
        }
        
        if ((collision.gameObject.CompareTag("Trampoline") || collision.gameObject.CompareTag("Sliding")) && specialtype == SpecialType.Sulfur)
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        else if((!collision.gameObject.CompareTag("Trampoline") && !collision.gameObject.CompareTag("Sliding") && specialtype == SpecialType.Sulfur))
        {
            Destroy(gameObject);
        }
        if(collision.gameObject.CompareTag("Ice") && specialtype == SpecialType.Sulfur)
        {
            Animator animator = collision.gameObject.GetComponent<Animator>();
            animator.SetBool("ismelting", true);
            Destroy(collision.gameObject, animator.GetCurrentAnimatorStateInfo(0).length + 1f);
        }
        
        if(collision.gameObject.CompareTag("Ground") && specialtype == SpecialType.liquidNitrogen)
        {
            Debug.Log(collision.gameObject.name);
            transform.rotation = Quaternion.Euler(0, 0, 0);
            intosliding = transform.localScale;
            intosliding = new Vector3(12, 1, 12);
            transform.localScale = intosliding;
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<Jellyspecial>());


        }
        else if (!collision.gameObject.CompareTag("Ground") && specialtype == SpecialType.liquidNitrogen)
        {
            Debug.Log(collision.gameObject.name);
            Destroy(gameObject);
        }              
    }
    

}
